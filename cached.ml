(* $Id$ *)

(* Sample main program for cache server *)

let main() =
  let (opt_list, cmdline_cfg) = Netplex_main.args() in

  Arg.parse
    opt_list
    (fun s -> raise (Arg.Bad ("Don't know what to do with: " ^ s)))
    "usage: cached [options]";

  Sys.set_signal Sys.sigpipe Sys.Signal_ignore;

  let factory() =
    Cache_plex.factory 
      ~name:"cached"
      ~post_start_hook:(fun cache cont ->
			  (* Tune O'Caml GC *)
			  let ctrl = Gc.get() in
			  Gc.set { ctrl with 
				     Gc.minor_heap_size = 128 * 1024;
				     Gc.space_overhead = 10;
				     Gc.max_overhead = 100;
				 }
		       )
      ()
  in
  Netplex_main.startup
    ( Netplex_mp.mp() )   (* multi-processing *)
    Netplex_log.logger_factories   (* allow all built-in logging styles *)
    Netplex_workload.workload_manager_factories (* ... all ways of workload management *)
    [ factory ()]
    cmdline_cfg
;;


main();;

