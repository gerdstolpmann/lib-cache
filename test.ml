#use "topfind";;
#require "netplex";;
#load "cache_common.cma";;
#load "cache_client.cma";;
open Cache_client;;
let config = object method buckets = [| Rpc_client.Inet("gate", 3333) |] method query_timeout = 1.0 method idle_timeout = 30.0 end;;
let c = create_sync_client config;;

let fill() =
  let tmo = Int64.add (Int64.of_float(Unix.gettimeofday())) 100L in
  for n = 1 to 5000 do
    let s = string_of_int n in
    c # set (`String s) s tmo 
      { opt_overwrite=false; 
	opt_add=true; 
	opt_undelete=false; 
	opt_setifchanged=false
      }
  done
;;


let gf n =
  let s = string_of_int n in
  c # get (`String s) 
    { opt_novalue = false; opt_getifmodifiedsince= None; opt_getifnotmd5=None}
;;

Rpc_client.verbose true;;
