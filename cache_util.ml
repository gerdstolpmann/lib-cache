(* $Id$ *)

type key =
    [ `Hash of Digest.t 
    | `String of string
    ]


let hash_of_key k =
  match k with
    | `Hash s -> s
    | `String s -> Digest.string s


let bucket_of_hash q h =
  (* We take the last 63 bits of the h string modulo q. Using int64 so
   * it also runs on Gerd's 32 bit laptop.
   *)
  assert(String.length h = 16);
  let hid_63 =
    Int64.logand
      (Netnumber.logical_int64_of_uint8
         (Netnumber.BE.read_string_uint8 h 8))
      0x7fff_ffff_ffff_ffffL in
  Int64.to_int (Int64.rem hid_63 (Int64.of_int q))


