(* $Id$ *)

(** Cache server *)

(** Configuration data for a server *)
class type cache_config =
object
  method directory : string
    (** The directory where the cache contents are stored as file *)

  method max_size : int64
    (** The (initial) maximum size in bytes *)

  method save_cache_period : int
    (** Save the cache to disk every this number of seconds (0 disables) *)

  method save_cache_speed : int
    (** Write this number of bytes per second (0 means as fast as possible) *)

  method rpc_program_number : int32
    (** The program number to use in the RPC protocol *)
end


type cache_instance
  (** A cache server instance *)


val create : cache_config -> cache_instance
  (** Creates a cache instance for the passed [cache_config] *)


val bind : Rpc_server.t -> cache_instance -> unit
  (** Binds the cache procedures in the passed RPC server. One can bind the
    * cache to several servers, or even to no servers if only local accesses
    * happen.
   *)

val activate : Unixqueue.event_system -> cache_instance -> unit
  (** Activates the cache: Loads contents from files, and establishes
    * timers. Must be called after [bind].
   *)

val save : cache_instance -> unit
  (** Saves the complete instance immediately to disk *)


val verify : log:(string -> unit) -> cache_instance -> unit
  (** Runs a routine that checks the structure of the cache instance.
    * Messages are printed to [log].
   *)

type full_key =
    { key : Cache_client.key;
      modulo : int;
    }
  (** The key and the modulo (number of servers) *)

val set_directly : cache_instance ->
          full_key ->
          string ->
          Cache_client.timestamp ->
          Cache_client.set_options ->
            [`Stored|`Not_stored] 
  (** Modifies the cache directly *)

val get_directly : cache_instance ->
          full_key ->
          Cache_client.get_options ->
            [`Found of Cache_client.entry|`Not_found]
  (** Reads from the cache directly *)

val delete_directly : cache_instance ->
             full_key ->
             Cache_client.timestamp ->
             Cache_client.delete_options ->
               unit
  (** Deletes directly in the cache *)

val clear_directly : cache_instance -> unit
  (** Clears the whole cache directly *)

val get_config : cache_instance -> Cache_client.config
  (** Get the cache's config *)

val set_config : cache_instance -> Cache_client.config -> unit
  (** Set the cache's config *)

val get_stats : cache_instance -> Cache_client.stats
  (** Get the statistics *)

val clear_counters : cache_instance -> unit
  (** Reset the statistics *)


val shutdown : cache_instance -> unit
  (** Shuts the instance down. 
    * This deletes any timers, and stops any save operations currently
    * being performed. It does not stop the RPC servers, however, because
    * the cache instance does not remember them, i.e. it is the task of
    * the user of this module to stop the RPC servers to which the
    * instance has been bound.
   *)
