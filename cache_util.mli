(* $Id$ *)

(** Utility functions *)

type key =
    [ `Hash of Digest.t
    | `String of string
    ]
    (** Keys of hash entries can be given in two ways:
      * - [`Hash dg]: As digest of a string
      * - [`String s]: As string (to be digested)
     *)


val hash_of_key : key -> string
  (** Compute the hash value of a key *)

val bucket_of_hash : int -> string -> int
  (** Compute the bucket number of [n], the number of buckets, and the
      hash value (which must be a string of 16 bytes).
   *)
