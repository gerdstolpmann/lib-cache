(* $Id$ *)

(** Run a cache server inside [Netplex] *)

val factory : 
      ?post_start_hook:(Cache_server.cache_instance -> Netplex_types.container -> unit) ->
      ?setup:(Rpc_server.t -> Cache_server.cache_instance -> unit) ->
      ?rpc_enable:bool -> 
      ?rpc_program_number:int32 ->
      name:string -> 
      unit -> 
        Netplex_types.processor_factory
  (** A netplex factory for the cache server.
    *
    * Accepts the following config parameters inside the "processor" 
    * section of the config file:
    * - cache_directory     (required, must be absolute path)
    * - cache_max_size      (required, in bytes)
    * - save_cache_period   (required, in seconds, use 0 to disable automatic save)
    * - save_cache_speed    (required, in bytes/s, use 0 for "as fast as possible")
    *
    * The [name] argument is the name of the processor.
    *
    * The [post_start_hook] is useful to register the service after startup.
    * The [setup] function can be used to bind further services to the
    * RPC server. By passing [rpc_enable:false] the function does not bind
    * any RPC service by itself.
    *
    * The server must run in only a single process/thread, i.e. the 
    * workload manager must be
    * {[
    *     workload_manager {
    *       type = "constant";
    *       threads = 1;
    *     }
    * ]}
    *
    * The cache server is compatible with both multi-processing and 
    * multi-threading. It is possible to use the same processor several
    * times in the same program in order to create several independent
    * caches.
    *
    * For a full example of netplexing the cache server see the source code
    * of [cached.ml] and the config file [cached.conf].
   *)
