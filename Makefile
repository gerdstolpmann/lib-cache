.PHONY: build all doc install release clean postconf release tag

build:
	omake

all:
	omake

doc:
	omake doc

install:
	omake install

clean:
	omake clean

postconf:
	omake postconf

release:
	omake release

tag:
	omake tag
